## Gitlab Runner for local testing

If you want to test your gitlab ci pipeline locally, you need a gitlab runner running and execute `gitlab-runner exec {runner_type} {job_name}`.

This tool integrates the docker typed gitlab runner and execution script for saving time.

### Usage
```shell script
$ export WORKING_DIR=/absolute/path/to/project
$ docker-compose up -d --build
$ docker-compose exec runner run [job_name]
```
The `.gitlab-ci.yml` must be there under the `WORKING_DIR`

### Limitations
- Can not used for jobs which extend the job templates
- Can not used for ci files which include the other files
- https://docs.gitlab.com/runner/commands/README.html#limitations-of-gitlab-runner-exec